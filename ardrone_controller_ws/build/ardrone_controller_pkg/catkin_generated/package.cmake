set(_CATKIN_CURRENT_PACKAGE "ardrone_controller_pkg")
set(ardrone_controller_pkg_VERSION "0.0.0")
set(ardrone_controller_pkg_MAINTAINER "fofo <fofo@todo.todo>")
set(ardrone_controller_pkg_PACKAGE_FORMAT "2")
set(ardrone_controller_pkg_BUILD_DEPENDS "ardrone_autonomy" "depth_image_proc" "depthimage_to_laserscan" "gazebo_msgs" "geometry_msgs" "rospy" "std_msgs")
set(ardrone_controller_pkg_BUILD_EXPORT_DEPENDS "ardrone_autonomy" "depth_image_proc" "depthimage_to_laserscan" "gazebo_msgs" "geometry_msgs" "rospy" "std_msgs")
set(ardrone_controller_pkg_BUILDTOOL_DEPENDS "catkin")
set(ardrone_controller_pkg_BUILDTOOL_EXPORT_DEPENDS )
set(ardrone_controller_pkg_EXEC_DEPENDS "ardrone_autonomy" "depth_image_proc" "depthimage_to_laserscan" "gazebo_msgs" "geometry_msgs" "rospy" "std_msgs")
set(ardrone_controller_pkg_RUN_DEPENDS "ardrone_autonomy" "depth_image_proc" "depthimage_to_laserscan" "gazebo_msgs" "geometry_msgs" "rospy" "std_msgs")
set(ardrone_controller_pkg_TEST_DEPENDS )
set(ardrone_controller_pkg_DOC_DEPENDS )
set(ardrone_controller_pkg_URL_WEBSITE "")
set(ardrone_controller_pkg_URL_BUGTRACKER "")
set(ardrone_controller_pkg_URL_REPOSITORY "")
set(ardrone_controller_pkg_DEPRECATED "")
#!/usr/bin/env python

import rospy
from gazebo_msgs.srv import GetModelState
from std_msgs.msg import Empty 
from geometry_msgs.msg import Twist
import numpy as np

# 4 parts - get user input locatio ,take off, get model state and navigate # 

## 1 get user input destination ##
def getInputs():
    print ("Please enter your x, yand z coordinates for your destination...")
    dest = [input(),input(),input()] #x,y,z
    print ("your destination coordinates are x: {0} y: {1} z: {2}".format(dest[0],dest[1],dest[2]))
    return dest 



## 2 take off & land##
def takeoff():
    # Starts a new node
    rospy.init_node('robot_pilot', anonymous=True)
    #publishers 
    takeoff_publisher = rospy.Publisher('/ardrone/takeoff',Empty, queue_size=10) 
    #land_publisher = rospy.Publisher('/ardrone/land',Empty, queue_size=10)
    #set rate
    r = rospy.Rate(10)      # 2. Sleeping and rates - http://library.isr.ist.utl.pt/docs/roswiki/rospy(2f)Overview(2f)Time.html#Sleeping_and_Rates
    #get timer ready        # 1.1.1 Time Zero - http://library.isr.ist.utl.pt/docs/roswiki/rospy(2f)Overview(2f)Time.html#Sleeping_and_Rates
    t0 = rospy.Time.now().to_sec()
    while (t0<1):
        t0 = rospy.Time.now().to_sec()
    t1 = t0 
    
    #takeoff
    while (t1-t0 < 2):
        takeoff_publisher.publish(Empty())
        t1 = rospy.Time.now().to_sec()
        r.sleep()

def land():
    # Starts a new node
    rospy.init_node('robot_pilot', anonymous=True)
    #publishers  
    land_publisher = rospy.Publisher('/ardrone/land',Empty, queue_size=10)
    #set rate
    r = rospy.Rate(10)      # 2. Sleeping and rates - http://library.isr.ist.utl.pt/docs/roswiki/rospy(2f)Overview(2f)Time.html#Sleeping_and_Rates
    #get timer ready        # 1.1.1 Time Zero - http://library.isr.ist.utl.pt/docs/roswiki/rospy(2f)Overview(2f)Time.html#Sleeping_and_Rates
    t0 = rospy.Time.now().to_sec()
    while (t0<1):
        t0 = rospy.Time.now().to_sec()
    t1 = t0 
    
    #land
    while (t1-t0 < 2):
        land_publisher.publish(Empty())
        t1 = rospy.Time.now().to_sec()
        r.sleep()



## 3 Get model state ##
def get_gazebo_model_state():
    try:
    	location = []
        models_coordinates = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
        resp_coordinates = models_coordinates('quadrotor', '')
        location.append(resp_coordinates.pose.position.x)
        location.append(resp_coordinates.pose.position.y)
        location.append(resp_coordinates.pose.position.z)   
        #print str(location)[1:-1]

    except rospy.ServiceException as e:
        rospy.loginfo("Get Model State service call failed:  {0}".format(e))

    return location
        


## 4 PID navigate ## 
def PID_nav(dest):
	rospy.init_node('robot_pilot', anonymous=True)
	velocity_publisher = rospy.Publisher('/cmd_vel', Twist, queue_size=10)
	vel_msg = Twist()
	model_state = np.asarray(get_gazebo_model_state())
	rate = rospy.Rate(15) 

	x_error_integral = 0 
	y_error_integral = 0 
	z_error_integral = 0 

	#warm up timer
	t0 = rospy.Time.now().to_sec()
	while (t0<1):
		t0 = rospy.Time.now().to_sec()
	t1 = t0 

	x_error = 0.51
	x_error_old = 0

	y_error = 0.51
	y_error_old = 0  

	z_error = 1.1
	z_error_old = 0 

	#get model into control state 
	for i in range(5):
		rate.sleep()
		vel_msg.linear.x = 0
		vel_msg.linear.y = 0
		vel_msg.linear.z = 0
		velocity_publisher.publish(vel_msg)
		#Force the robot to stop
	

	
    	
	while  abs(x_error) > 0.5 or abs(y_error) > 0.5 or abs(z_error) > 1:
		rate.sleep()

		vel_msg.linear.z = 0
		vel_msg.angular.x = 0
		vel_msg.angular.y = 0
		vel_msg.angular.z = 0

		model_state = np.asarray(get_gazebo_model_state())
		#print ("model_state: ", model_state) #+


		kp = -1
		ki = -0.01
		kd = -0.12

		threshold = 20


		## PID x ##
		#error# 
		x_error = model_state [0] - dest [0]
		#Proportional#
		P_x = kp * x_error
		#Integral - past compensation#
		x_error_integral += x_error 
		I_x = ki * x_error_integral
		#Derivative - future compensation#
		t1 = rospy.Time.now().to_sec()
		delta_t = t1-t0
		delta_x_error = x_error - x_error_old
		D_x = kd * (delta_x_error / delta_t)

		#PID summation# 
		vel_msg.linear.x = P_x + I_x + D_x
		# threshold to stop overshoot / orbital behaviour 
		# if (vel_msg.linear.x > threshold or vel_msg.linear.x < -threshold ):
		# 	vel_msg.linear.x = vel_msg.linear.x/2 



		## PID y ##
		#error#
		y_error = model_state [1] - dest [1]
		#Proportional#
		P_y = kp * y_error
		#Integral - past compensation#
		y_error_integral += y_error 
		I_y = ki * y_error_integral
		#Derivative - future compensation#
		t1 = rospy.Time.now().to_sec()
		delta_t = t1-t0
		delta_y_error = y_error - y_error_old
		D_y = kd * (delta_y_error / delta_t)

		#PID summation# 
		vel_msg.linear.y = P_y + I_y + D_y
		#threshold to stop overshoot / orbital behaviour 
		# if (vel_msg.linear.y > threshold or vel_msg.linear.y < -threshold ):
		# 	vel_msg.linear.y = vel_msg.linear.y/2

		## PID z ##
		#error#
		z_error = model_state [2] - dest [2]
		#Proportional#
		P_z = kp * z_error
		#Integral - past compensation#
		z_error_integral += z_error 
		I_z = ki * z_error_integral
		#Derivative - future compensation#
		t1 = rospy.Time.now().to_sec()
		delta_t = t1-t0
		delta_z_error = z_error - z_error_old
		D_z = kd * (delta_z_error / delta_t)

		#PID summation# 
		vel_msg.linear.z = P_z + I_z + D_z 
		#threshold to stop overshoot / orbital behaviour 
		# if (vel_msg.linear.z > threshold or vel_msg.linear.z < -threshold ):
		# 	vel_msg.linear.z = vel_msg.linear.z/2
		## Keep the drone airborne if destination coordinate z is < 1 
		if (model_state[2] < 0.5 and (abs(x_error) > 0.085 or abs(y_error) > 0.085)):
			vel_msg.linear.z = abs(vel_msg.linear.z) + 5


		##PUBLISH VEL##
		velocity_publisher.publish(vel_msg)
		

		#update timer# 
		t0 = rospy.Time.now().to_sec()

		##Live Log## 
		print ("destination: ", dest)
		print ("model_state: ", model_state)
		print("\n")
		print ("x_error :",x_error) #+
		print ("y_error :",y_error) #+
		print ("z_error :",z_error) #+
		print("\n")
		print ("vel_msg", vel_msg)
		print("\n")

	## Land drone once at destination ## 
	rate.sleep()	
	#After the loop, stops the robot
	vel_msg.linear.x = 0
	vel_msg.linear.y = 0
	vel_msg.linear.z = 0
	#Force the robot to stop
	velocity_publisher.publish(vel_msg)
		
	print ("Arrived at destination !!!")
	print ("now landing...")
	land() 




##########
## MAIN ## 
##########

if __name__ == '__main__':
    try:

    	## get user input## 
    	dest = np.asarray(getInputs()) 
    	## take off #
    	takeoff() 
        ##PID nav##
        PID_nav(dest)
        ## land #
        


    except rospy.ROSInterruptException: pass
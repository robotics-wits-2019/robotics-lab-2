#!/usr/bin/env python
#chmod u+x ~/<filename>
import rospy
from std_msgs.msg import Empty 

def takeoff_land():
    # Starts a new node
    rospy.init_node('robot_pilot', anonymous=True)
     
    #publishers 
    takeoff_publisher = rospy.Publisher('/ardrone/takeoff',Empty, queue_size=10) 
    land_publisher = rospy.Publisher('/ardrone/land',Empty, queue_size=10)
    #set rate
    r = rospy.Rate(10)      # 2. Sleeping and rates - http://library.isr.ist.utl.pt/docs/roswiki/rospy(2f)Overview(2f)Time.html#Sleeping_and_Rates
    #get timer ready        # 1.1.1 Time Zero - http://library.isr.ist.utl.pt/docs/roswiki/rospy(2f)Overview(2f)Time.html#Sleeping_and_Rates
    t0 = rospy.Time.now().to_sec()
    while (t0<1):
        t0 = rospy.Time.now().to_sec()
    t1 = t0 
    
     
    #takeoff
    while (t1-t0 < 3):
        takeoff_publisher.publish(Empty())
        t1 = rospy.Time.now().to_sec()
        r.sleep()

    #land
    t0 = t1
    while (t1-t0 < 2):
        land_publisher.publish(Empty())
        t1 = rospy.Time.now().to_sec()
        r.sleep()
              


if __name__ == '__main__':
    try:
        #Testing our function
        takeoff_land()
    except rospy.ROSInterruptException: pass

#!/usr/bin/env python
#chmod u+x *.py
#https://www.youtube.com/watch?v=WqK2IY5_9OQ
from gazebo_msgs.srv import GetModelState
import rospy

class model:
    def __init__(self, name, relative_entity_name):
        self._name = name
        self._relative_entity_name = relative_entity_name

class modelsInfo:

    _modelListDict = {
        'model_1': model('quadrotor', ''),
        'model_2': model('House 1', 'link'),
        'model_3': model('House 2', 'link'),
        'model_4': model('nist', 'nist_maze_wall_triple_holes_120_link'),
        'model_5': model('House 3', 'link'),
        'model_6': model('ground_plane', 'link'),
        'model_7': model('jersey_barrier', 'link'),
        'model_8': model('grey_wall', 'link'),

    }

    def show_gazebo_models(self):
        try:
            models_coordinates = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
            for model in self._modelListDict.itervalues():
                resp_coordinates = models_coordinates(model._name, model._relative_entity_name)
                
                #print values 
                print '\n'
                print 'Status.success = ', resp_coordinates.success
                print(model._name)

                print 'position'
                print("X : " + str(resp_coordinates.pose.position.x))
                print("Y : " + str(resp_coordinates.pose.position.y))
                print("Z : " + str(resp_coordinates.pose.position.z))

                print 'orientation'
                print("Quaternion X : " + str(resp_coordinates.pose.orientation.x))
                print("Quaternion Y : " + str(resp_coordinates.pose.orientation.y))
                print("Quaternion Z : " + str(resp_coordinates.pose.orientation.z))

        except rospy.ServiceException as e:
            rospy.loginfo("Get Model State service call failed:  {0}".format(e))



if __name__ == '__main__':
    try:
        #Testing our function
        mInfo = modelsInfo()
    	mInfo.show_gazebo_models()
    except rospy.ROSInterruptException: pass